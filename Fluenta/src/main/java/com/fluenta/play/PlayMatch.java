package com.fluenta.play;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fluenta.entity.Goal;
import com.fluenta.entity.Match;
import com.fluenta.entity.Player;
import com.fluenta.entity.Team;
import com.fluenta.enums.MatchResultStatus;
import com.fluenta.enums.Position;
import com.fluenta.models.Pairs;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class PlayMatch {
	
	private final int MIN = 3;
	private final int MAX = 9;
	
	@Autowired
	private MatchHelper matchHelper;
	
	
	
	public void playMatch(Pairs pairs) {		
		Match currentMatch = new Match();		
		
		int homeTeamScoreChances = getScoreChanceNumber() +1;
		int awayTeasmScoreChances = getScoreChanceNumber();
		
		int homeTeamStrength = matchHelper.getTeamStrengthById(pairs.getHomeTeamId());
		int awayTeamStrength = matchHelper.getTeamStrengthById(pairs.getAwayTeamId());
		
		if(homeTeamStrength < awayTeamStrength) {
			awayTeasmScoreChances++;
		} else if(homeTeamStrength > awayTeamStrength) {
			homeTeamScoreChances++;
		}
		log.info("Teams goal positions home: {}, away:{}.", homeTeamScoreChances, awayTeasmScoreChances);
		
		currentMatch = startMatch(currentMatch, pairs, homeTeamScoreChances, awayTeasmScoreChances);
		
		finishMatch(currentMatch);
		
	}
	
	private int getScoreChanceNumber() {
		return ThreadLocalRandom.current().nextInt(MIN, MAX + 1);
	}
	
	private Match startMatch(Match currentMatch, Pairs pairs, int homeTeamScoreChances, int awayTeasmScoreChances) {
		log.info("Start the match!");
		
		currentMatch = new Match(pairs.getHomeTeamId(), pairs.getAwayTeamId(), 0, 0);
		currentMatch = matchHelper.saveMatch(currentMatch);
		
		simulationOFHomeTeamAttacks(currentMatch, pairs, homeTeamScoreChances);
		
		simulationOFawayTeamAttacks(currentMatch, pairs, awayTeasmScoreChances);
		
		return currentMatch;
		
	}
	
	private void simulationOFHomeTeamAttacks(Match currentMatch, Pairs pairs, int homeTeamScoreChances) {
		log.info("Simulating home team attacks!");
		
		log.info("home team chances {}!", homeTeamScoreChances);
		
		Player player;
		int score;
		boolean attackResult;
		
		for(int numberOfAttacks=0; numberOfAttacks<homeTeamScoreChances; ++numberOfAttacks) {
			player = matchHelper.getOffensivePlayer(pairs.getHomeTeamId());

			score = player.getPlayerStrength();
			score += (score + 5 <= 100) ? 5 : 100-score;
			player.setPlayerStrength(score);

			attackResult = false;
			
			if(player.getPosition().equals(Position.MIDFIELDER)) {
				log.info("Midfielder choosen {}!", player);
				attackResult = midfielderChosen(player, pairs.getAwayTeamId(), numberOfAttacks);
			} else {
				log.info("Attacker choosen {}!", player);
				attackResult = attackerChosen(player, pairs.getAwayTeamId(), numberOfAttacks);
			}
		
			if(attackResult) {
				log.info("Goal scored! player: {}", player);
				Goal goal = matchHelper.saveGoal(new Goal(player.getPlayerId(),
						currentMatch.getId(),
						player.getTeam_id(),
						pairs.getAwayTeamId()));
				log.info("Goal saved! Data: {}", goal);
				
				currentMatch.setHomeTeamScore(currentMatch.getHomeTeamScore() + 1);
			} else {
				log.info("Goal score chance missed by player: {}!", player);
			}
		}
	}
	
	private void simulationOFawayTeamAttacks(Match currentMatch, Pairs pairs, int teamScoreChances) {
		log.info("Simulating away team attacks!");
		
		log.info("Away team chances {}!", teamScoreChances);
		
		Player player;
		int score;
		boolean attackResult;
		
		for(int numberOfAttacks=0; numberOfAttacks<teamScoreChances; ++numberOfAttacks) {
			player = matchHelper.getOffensivePlayer(pairs.getAwayTeamId());

			score = player.getPlayerStrength();
			score += (score + 5 <= 100) ? 5 : 100-score;
			player.setPlayerStrength(score);

			attackResult = false;
			
			if(player.getPosition().equals(Position.MIDFIELDER)) {
				log.info("Midfielder choosen {}!", player);
				attackResult = midfielderChosen(player, pairs.getHomeTeamId(), numberOfAttacks);
			} else {
				log.info("Attacker choosen {}!", player);
				attackResult = attackerChosen(player, pairs.getHomeTeamId(), numberOfAttacks);
			}
			
			log.info("Attack result: {}!", attackResult);
		
			if(attackResult) {
				log.info("Goal scored! player: {}", player);
				Goal goal = matchHelper.saveGoal(new Goal(player.getPlayerId(),
						currentMatch.getId(),
						player.getTeam_id(),
						pairs.getHomeTeamId()));
				log.info("Goal saved! Data: {}", goal);
				
				currentMatch.setAwayTeamScore(currentMatch.getHomeTeamScore() + 1);
			} else {
				log.info("Goal score chance missed by player: {}!", player);
			}
		}
	}
	
	
	
	private boolean midfielderChosen(Player player, Long defenderTeamId, int numberOfAttacks) {
		int rand = ThreadLocalRandom.current().nextInt(1, 11);
		
		log.info("Midfielder first rand number: {}", rand);
		
		if(rand < 5) {
			return fightWithAGoalie(player, defenderTeamId, numberOfAttacks);
		} else if(rand < 7) {
			if(!fightWithAMidfielder(player, defenderTeamId, numberOfAttacks)) return false;
			if(!fightWithADefender(player, defenderTeamId, numberOfAttacks)) return false;
			return fightWithAGoalie(player, defenderTeamId, numberOfAttacks);
		} else if(rand < 10) {
			if(!fightWithADefender(player, defenderTeamId, numberOfAttacks)) return false;
			return fightWithAGoalie(player, defenderTeamId, numberOfAttacks);
		} else {
			return false;
		}

	}
	
	private boolean attackerChosen(Player player, Long defenderTeamId, int numberOfAttacks) {
		int rand = ThreadLocalRandom.current().nextInt(1, 11);
		
		log.info("Attacker first rand number: {}", rand);
		
		if(rand < 3) {
			return fightWithAGoalie(player, defenderTeamId, numberOfAttacks);
		} else if(rand < 11) {
			if(!fightWithADefender(player, defenderTeamId, numberOfAttacks)) return false;
			return fightWithAGoalie(player, defenderTeamId, numberOfAttacks);
		}
		
		return true;
	}
	
	private boolean fightWithAMidfielder(Player player, Long defenderTeamId, int numberOfAttacks) {
		log.info("Fight with midfielder!");
		if(youngerPlayerHandicap(player)) return false;
		
		Player midfielder = matchHelper.getMidFielder(defenderTeamId);
		
		return fight(player, midfielder, numberOfAttacks);
		
		
	}
	
	private boolean fightWithADefender(Player player, Long defenderTeamId, int numberOfAttacks) {
		log.info("Fight with defender!");
		if(youngerPlayerHandicap(player)) return false;
		
		Player defender = matchHelper.getDefender(defenderTeamId);
		
		return fight(player, defender, numberOfAttacks);
	}
	
	private boolean fightWithAGoalie(Player player, Long defenderTeamId, int numberOfAttacks) {
		log.info("Fight with goalie!");
		if(youngerPlayerHandicap(player)) return false;
		
		Player goalie = matchHelper.getGoalie(defenderTeamId);
		
		return fight(player, goalie, numberOfAttacks); 
		
	}
	
	private boolean fight(Player attacker, Player defender, int numberOfAttacks) {
		log.info("Start fight!");
		log.info("Attacker: {}, defender: {}!", attacker, defender);
		int chance = calculateChanceToAttackerWin(attacker, defender, numberOfAttacks);
		
		log.info("Calculated attacker chance before fight: {}!", chance);
		
		int rand = ThreadLocalRandom.current().nextInt(1, 101);
		
		log.info("Fight rand number: {}!", rand);
		
		if(rand<=chance) {
			log.info("Attacker win!");
			return true;
		} else {
			log.info("Attacker lose!");
			return false;
		}
	}
	
	private boolean youngerPlayerHandicap(Player player) {
		int chanceToKickHisOwnLeg;
		
		if(player.getAge() < 22) {
			chanceToKickHisOwnLeg = 22 - player.getAge();
			int rand = ThreadLocalRandom.current().nextInt(1, 11);
			log.info("Young player first rand number: {}", rand);
			if(rand <= chanceToKickHisOwnLeg) {
				log.info("Young player kick his own leg! {}", player);
				return true;
			}
		} 
		
		return false;
	}
	
	private int calculateChanceToAttackerWin(Player attacker, Player defender, int numberOfAttacks) {
		log.info("Chance calculating!");
		int difference = Math.abs(attacker.getPlayerStrength() - defender.getPlayerStrength())/2;
		
		log.info("Difference in %: {}!",difference);
		
		int chance = 50;
		
		log.info("Attacker strenght: {}, defender strenght: {}!", attacker.getPlayerStrength(), defender.getPlayerStrength());		
		
		if(attacker.getPlayerStrength() > defender.getPlayerStrength()) {
			chance += difference;
		} else {
			chance -= difference;
		}
		
		log.info("Modified chance: {}!", chance);
		
		if(!attacker.getLeg().equals(defender.getLeg()) && !defender.getPosition().equals(Position.GOALKEEPER)) {
			chance -= 10;
			log.info("Modified chance after different leg: {}!", chance);
		}
		
		if(33 <= attacker.getAge() && attacker.getAge() < 38) {
			chance -= 2*numberOfAttacks*(attacker.getAge()-32);
			log.info("Modified chance after old player: {}!", chance);
		}
		
		return chance < 1 ? 1 : chance;
				
	}
	
	private void finishMatch(Match currentMatch) {
		log.info("Start saving match!");
		if(currentMatch.getHomeTeamScore() > currentMatch.getAwayTeamScore()) {
			currentMatch.setResultStatus(MatchResultStatus.WIN.toString());
			currentMatch.setWinnerTeamId(currentMatch.getHomeTeamId());
		} else if (currentMatch.getHomeTeamScore() < currentMatch.getAwayTeamScore()) {
			currentMatch.setResultStatus(MatchResultStatus.WIN.toString());
			currentMatch.setWinnerTeamId(currentMatch.getAwayTeamId());
		} else {
			currentMatch.setResultStatus(MatchResultStatus.DRAW.toString());
		}
		
		log.info("Save Match with result. Match :{}!", currentMatch);
		matchHelper.saveMatch(currentMatch);
	}
	
	public Long getPlayedMatchesNumber() {
		return matchHelper.getPlayedMatchesNumber();
	}
	
	public Long getPlayedMatchesNumber(Long teamId) {
		return matchHelper.getPlayedMatchesNumber(teamId);
	}
	
	public Long getWinnerMatchNumber(Long teamId) {
		return matchHelper.getWinnerMatchNumber(teamId);
	}
	
	public Long getDrawMatchNumber(Long teamId) {
		return matchHelper.getDrawMatchNumber(teamId);
	}
	
	public Long getTeamPoints(Long teamId) {
		return matchHelper.getDrawMatchNumber(teamId) + matchHelper.getWinnerMatchNumber(teamId)*3;
	}
	
	public Long getGoalScoredNumber(Long teamId) {
		return matchHelper.getGoalScoredNumber(teamId);
	}
	
	public Long getGoalReceivedNumber(Long teamId) {
		return matchHelper.getGoalReceivedNumber(teamId);
	}
	
	public Team getTeam(Long teamId) {
		return matchHelper.getTeamById(teamId);
	}
	
	public List<Player> getMVP( ) {
		List<Long> ids = matchHelper.getMVP();
		
		List<Player> players = new ArrayList<Player>();
		
		for(Long id : ids) {
			
			Player player = matchHelper.getPlayerById(id);
			
			players.add(player);
		}
		
		return players;
	}
}
