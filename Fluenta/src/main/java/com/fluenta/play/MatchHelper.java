package com.fluenta.play;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fluenta.entity.Goal;
import com.fluenta.entity.Match;
import com.fluenta.entity.Player;
import com.fluenta.entity.Team;
import com.fluenta.exception.GoalRepositoryException;
import com.fluenta.exception.MatchRepositoryException;
import com.fluenta.exception.PlayerRepositoryException;
import com.fluenta.exception.TeamRepositoryException;
import com.fluenta.repository.GoalRepository;
import com.fluenta.repository.MatchRepository;
import com.fluenta.repository.PlayerRepository;
import com.fluenta.repository.TeamRepository;

@Service
public class MatchHelper {

	@Autowired
	private TeamRepository teamRepository;
	
	@Autowired
	private PlayerRepository playerRepository;
	
	@Autowired
	private GoalRepository goalRepository;
	
	@Autowired
	private MatchRepository matchRepository;
	
	public Team getTeamById(Long id) {
		return teamRepository.findById(id).orElseThrow(() -> 
		new TeamRepositoryException("Team not found by id: "+id));
	}
	
	public int getTeamStrengthById(Long id) {
		return teamRepository.getTeamStrengthById(id).orElseThrow(() -> 
		new TeamRepositoryException("Team strength not found by team id: "+id));
	}
	
	public Player getDefensivePlayer(Long team_id) {
		return playerRepository.getDefensivePlayer(team_id).orElseThrow(() -> 
		new PlayerRepositoryException("Defensive player not found, team_id: "+team_id));
	}

	public Player getOffensivePlayer(Long team_id) {
		return playerRepository.getOffensivePlayer(team_id).orElseThrow(() -> 
		new PlayerRepositoryException("Offensive player not found, team_id: "+team_id));
	}
	
	public Player getGoalie(Long team_id) {
		return playerRepository.getGoalie(team_id).orElseThrow(() -> 
		new PlayerRepositoryException("Goalie not found, team_id: "+team_id));
	}
	
	public Player getMidFielder(Long team_id) {
		return playerRepository.getMidFielder(team_id).orElseThrow(() -> 
		new PlayerRepositoryException("MidFielder not found, team_id: "+team_id));
	}
	
	public Player getDefender(Long team_id) {
		return playerRepository.getDefender(team_id).orElseThrow(() -> 
		new PlayerRepositoryException("Defender not found, team_id: "+team_id));
	}
	
	public Goal saveGoal(Goal goal) {
		try {
			return goalRepository.save(goal);
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
			throw new GoalRepositoryException("Goal saving failed!");
		}
	}
	
	public Match saveMatch(Match match) {
		try {
			return  matchRepository.save(match);
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
			throw new MatchRepositoryException("Match saving failed!");
		}
	}
	
	public Long getPlayedMatchesNumber() {
		try {
			return matchRepository.count();
		} catch (Exception e) {
			e.printStackTrace();
			throw new MatchRepositoryException("Get played match number failed!");
		}
	}
	
	public Long getPlayedMatchesNumber(Long teamId) {
		return matchRepository.getPlayedMatchNumber(teamId).orElseThrow(() -> 
		new MatchRepositoryException("Can't get played matches number, team id: "+teamId));
	}
	
	public Long getWinnerMatchNumber(Long teamId) {
		return matchRepository.getWinnerMatchNumber(teamId).orElseThrow(() -> 
		new MatchRepositoryException("Can't get won matches number, team id: "+teamId));
	}
	
	public Long getDrawMatchNumber(Long teamId) {
		return matchRepository.getDrawMatchNumber(teamId).orElseThrow(() -> 
		new MatchRepositoryException("Can't get draw matches number, team id: "+teamId));
	}
	
	public Long getGoalScoredNumber(Long teamId) {
		return goalRepository.getGoalScoredNumber(teamId).orElseThrow(() -> 
		new GoalRepositoryException("Can't get scored goals number, team id: "+teamId));
	}
	
	public Long getGoalReceivedNumber(Long teamId) {
		return goalRepository.getGoalReceivedNumber(teamId).orElseThrow(() -> 
		new GoalRepositoryException("Can't get received goals number, team id: "+teamId));
	}
	
	public List<Long> getMVP( ) {
		return goalRepository.getMVP();
	}
	
	public Player getPlayerById(Long playerId) {
		return playerRepository.findById(playerId).orElseThrow(() -> 
		new PlayerRepositoryException("Player not found, id: "+ playerId));
	}
}
