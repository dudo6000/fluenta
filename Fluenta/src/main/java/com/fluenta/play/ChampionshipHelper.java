package com.fluenta.play;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fluenta.exception.TeamRepositoryException;
import com.fluenta.repository.TeamRepository;

@Service
public class ChampionshipHelper {

	@Autowired
	private TeamRepository teamRepository;
	
	public List<Long> getTeamsIds() {
			
		return teamRepository.findAllId().orElseThrow(() -> new TeamRepositoryException("Can't get etams ids!") );
	}
}
