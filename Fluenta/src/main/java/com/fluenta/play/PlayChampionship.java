package com.fluenta.play;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fluenta.entity.Player;
import com.fluenta.entity.Team;
import com.fluenta.models.Pairs;
import com.fluenta.models.TeamResult;
import com.fluenta.models.TeamSortingComparator;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class PlayChampionship {
	
	@Autowired
	private PlayMatch playMatch;
	
	@Autowired
	private ChampionshipHelper championshipHelper;
	
	public void playChampionship() {
		log.info("Create matchlist.");
		
		List<Pairs> matches = createMatchesList();
		
		log.info("Start championship.");
		
		for(Pairs pairs : matches) {
			playMatch.playMatch(pairs);
		}
		
		log.info("Played matches: {}", playMatch.getPlayedMatchesNumber());
		
		log.info("Played matches team_id = 1 : {}", playMatch.getPlayedMatchesNumber(1L));
		
		log.info("Get winner matches Number of team_id = 1 : {}", playMatch.getWinnerMatchNumber(1L));
		
		log.info("Get Draw matches Number of team_id = 1 : {}", playMatch.getDrawMatchNumber(1L));
		
		log.info("Calculate team points of team_id = 1 : {}", playMatch.getTeamPoints(1L));
		
		log.info("Get team scored goals number of team_id = 1 : {}", playMatch.getGoalScoredNumber(1L));
		
		log.info("Get team reveived goals number of team_id = 1 : {}", playMatch.getGoalReceivedNumber(1L));
		
		log.info("Winner team : {}", getChampionshipWinner());
		
		log.info("MVP or MVPs : {}", getMVP());
		
		
	}
	
	private List<Pairs> createMatchesList() {
		
		List<Long> ids = championshipHelper.getTeamsIds();
		
		List<Pairs> matches = new ArrayList<>();
		
		for(Long id1 : ids) {
			for(Long id2 : ids) {
				if(id1.equals(id2)) continue;
				
				matches.add(new Pairs(id1, id2));
			}
		}
		
		log.info("Generated matchlist: {}", matches);
		
		return matches;
	}
	
	public Team getChampionshipWinner() {
		List<Long> ids = championshipHelper.getTeamsIds();
		
		List<TeamResult> teamResultList = new ArrayList<>();
		
		for(Long id : ids) {
			TeamResult teamResult = new TeamResult();
			teamResult.setTeamId(id);
			teamResult.setPoints(playMatch.getTeamPoints(id).intValue());
			teamResult.setMatchesWon(playMatch.getWinnerMatchNumber(id).intValue());
			teamResult.setScoredGoals(playMatch.getGoalScoredNumber(id).intValue());
			teamResult.setGoalsDifference(playMatch.getGoalScoredNumber(id).intValue()-playMatch.getGoalReceivedNumber(id).intValue());
			
			teamResultList.add(teamResult);
		}
		
		log.info("Unsorted Teams: {}", teamResultList);
		
		teamResultList.sort(new TeamSortingComparator());
		
		log.info("Sorted Teams: {}", teamResultList);
		
		return playMatch.getTeam(teamResultList.get(0).getTeamId());
	}
	
	public List<Player> getMVP() {
		return playMatch.getMVP();
	}
	
}
