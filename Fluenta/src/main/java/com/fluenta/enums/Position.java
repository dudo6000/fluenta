package com.fluenta.enums;

public enum Position {
	GOALKEEPER,
	DEFENDER,
	MIDFIELDER,
	ATTACKER
}
