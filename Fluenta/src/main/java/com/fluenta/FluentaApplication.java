package com.fluenta;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.fluenta.exception.GoalRepositoryException;
import com.fluenta.exception.MatchRepositoryException;
import com.fluenta.exception.PlayerRepositoryException;
import com.fluenta.exception.TeamRepositoryException;
import com.fluenta.play.PlayChampionship;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@SpringBootApplication
public class FluentaApplication implements CommandLineRunner {

	@Autowired
	private PlayChampionship playChampionship;
	
	public static void main(String[] args) {
		SpringApplication.run(FluentaApplication.class, args);
	}
	
	@Override
    public void run(String... args) throws Exception {

		log.info("Program started!");
		try {
			
			playChampionship.playChampionship();
			
		} catch (GoalRepositoryException e) {
			
			log.error("Goal repository exception catched");
			log.error("Program stop runnig!");
			
		} catch (MatchRepositoryException e) {
			
			log.error("Match repository exception catched");
			log.error("Program stop runnig!");
			
		} catch (PlayerRepositoryException e) {
			
			log.error("Player repository exception catched");
			log.error("Program stop runnig!");
			
		} catch (TeamRepositoryException e) {
			
			log.error("Team repository exception catched");
			log.error("Program stop runnig!");
			
		}
        
    }

}
