package com.fluenta.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.fluenta.entity.Player;

public interface PlayerRepository extends JpaRepository<Player, Long> {

	@Query(value = "SELECT * FROM PLAYERS a WHERE a.team_id = ?1 AND a.position = 'DEFENDER'  ORDER BY RAND() LIMIT 1", nativeQuery = true)
	Optional<Player> getDefensivePlayer(Long id);
	
	@Query(value = "SELECT * FROM PLAYERS a WHERE a.team_id = ?1 AND (a.position = 'MIDFIELDER' OR a.position = 'ATTACKER') ORDER BY RAND() LIMIT 1", nativeQuery = true) 
	Optional<Player> getOffensivePlayer(Long id);
	
	@Query(value = "SELECT * FROM PLAYERS a WHERE a.team_id = ?1 AND a.position = 'GOALKEEPER'", nativeQuery = true) 
	Optional<Player> getGoalie(Long id);
	
	@Query(value = "SELECT * FROM PLAYERS a WHERE a.team_id = ?1 AND a.position = 'MIDFIELDER' ORDER BY RAND() LIMIT 1", nativeQuery = true) 
	Optional<Player> getMidFielder(Long id);
	
	@Query(value = "SELECT * FROM PLAYERS a WHERE a.team_id = ?1 AND a.position = 'DEFENDER' ORDER BY RAND() LIMIT 1", nativeQuery = true) 
	Optional<Player> getDefender(Long id);
}
