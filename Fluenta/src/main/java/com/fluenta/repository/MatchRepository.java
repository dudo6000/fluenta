package com.fluenta.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.fluenta.entity.Match;

@Repository
public interface MatchRepository extends JpaRepository <Match, Long> {
	
	@Query("SELECT COUNT(*) FROM Match a WHERE a.homeTeamId=?1 OR a.awayTeamId=?1")
	Optional<Long> getPlayedMatchNumber(Long teamId);
	
	@Query("SELECT COUNT(*) FROM Match a WHERE a.winnerTeamId=?1")
	Optional<Long> getWinnerMatchNumber(Long teamId);
	
	@Query("SELECT COUNT(*) FROM Match a WHERE (a.homeTeamId=?1 OR a.awayTeamId=?1) AND a.resultStatus = 'DRAW'")
	Optional<Long> getDrawMatchNumber(Long teamId);
}
