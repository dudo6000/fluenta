package com.fluenta.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.fluenta.entity.Team;

@Repository
public interface TeamRepository  extends JpaRepository<Team, Long> {
	
	@Query("SELECT a.id FROM Team a")
	Optional<List<Long>> findAllId();
	
	@Query("SELECT a.teamStrength FROM Team a WHERE a.id=?1")
	Optional<Integer> getTeamStrengthById(Long id);
	
}
