package com.fluenta.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.fluenta.entity.Goal;

@Repository
public interface GoalRepository extends JpaRepository<Goal, Long> {

	@Query("SELECT COUNT(*) FROM Goal a WHERE a.playerTeamId=?1")
	Optional<Long> getGoalScoredNumber(Long teamId);
	
	@Query("SELECT COUNT(*) FROM Goal a WHERE a.sufferingTeamId=?1")
	Optional<Long> getGoalReceivedNumber(Long teamId);
	
	@Query(value ="SELECT PLAYER_ID FROM GOALS GROUP BY PLAYER_ID HAVING COUNT(*)=(SELECT COUNT(*) FROM GOALS GROUP BY PLAYER_ID ORDER BY COUNT(*) DESC LIMIT 1)", nativeQuery = true)
	List<Long> getMVP();
		
}
