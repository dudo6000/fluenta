package com.fluenta.exception;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class PlayerRepositoryException extends RuntimeException {

	private static final long serialVersionUID = 2040652089427382931L;

	public PlayerRepositoryException(String message) {
		super(message);
		log.error(message);
	}
}
