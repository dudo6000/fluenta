package com.fluenta.exception;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class GoalRepositoryException extends RuntimeException {

	private static final long serialVersionUID = 4750651269146927561L;
	
	public GoalRepositoryException(String message) {
		super(message);
		log.error(message);
	}

}
