package com.fluenta.exception;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class MatchRepositoryException extends RuntimeException {

	private static final long serialVersionUID = 4797468611658761956L;

	public MatchRepositoryException(String message) {
		super(message);
		log.error(message);
	}
}
