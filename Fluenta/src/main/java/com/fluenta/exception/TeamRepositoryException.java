package com.fluenta.exception;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class TeamRepositoryException extends RuntimeException {

	private static final long serialVersionUID = 9190506735564257702L;

	public TeamRepositoryException(String message) {
		super(message);
		log.error(message);
	}
}
