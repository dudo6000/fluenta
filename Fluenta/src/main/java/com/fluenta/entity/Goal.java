package com.fluenta.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Entity
@Table(name = "goals")
@Getter
@Setter
@ToString
public class Goal {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@NotNull
	private Long playerId;
	
	@NotNull
	private Long matchId;
	
	@NotNull
	private Long playerTeamId;
	
	@NotNull
	private Long sufferingTeamId;

	public Goal(@NotNull Long playerId, @NotNull Long matchId, @NotNull Long playerTeamId,
			@NotNull Long sufferingTeamId) {
		super();
		this.playerId = playerId;
		this.matchId = matchId;
		this.playerTeamId = playerTeamId;
		this.sufferingTeamId = sufferingTeamId;
	}
	
	
}
