package com.fluenta.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Entity
@Table(name = "matches")
@Getter
@Setter
@ToString
public class Match {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@NotNull
	private Long homeTeamId;
	
	@NotNull
	private Long awayTeamId;
	
	@NotNull
	private int homeTeamScore;
	
	@NotNull
	private int awayTeamScore;
	
	private String resultStatus;
	
	private Long winnerTeamId;

	public Match() {
		super();
	}
	
	public Match(@NotNull Long homeTeamId, @NotNull Long awayTeamId, @NotNull int homeTeamScore,
			@NotNull int awayTeamScore) {
		super();
		this.homeTeamId = homeTeamId;
		this.awayTeamId = awayTeamId;
		this.homeTeamScore = homeTeamScore;
		this.awayTeamScore = awayTeamScore;
	}

}
