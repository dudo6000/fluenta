package com.fluenta.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;


@Entity
@Table(name = "players")
@Getter
@Setter
@ToString
public class Player {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Long playerId;
	
	@NotNull
	private Long team_id;
	
	@NotNull
	private String playerName;
	
	@Min(16)
	@Max(38)
	private int age;
	
	@NotNull
	private String position;
	
	@NotNull
	private String leg;
	
	
	@NotNull
	@Min(1)
	@Max(100)
	private int playerStrength;
	
}
