package com.fluenta.entity;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Entity
@Table(name = "teams")
@Getter
@Setter
@ToString
public class Team {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Long teamId;
	
	@NotNull
	private String teamName;
	
	@NotNull
	private String location;
	
	@Min(1)
	@Max(1100)
	@NotNull
	private int teamStrength;
	

	@OneToMany(targetEntity = Player.class, fetch = FetchType.EAGER)
	@JoinColumn(name = "team_id", referencedColumnName = "id")
	private List<Player> players;

}
