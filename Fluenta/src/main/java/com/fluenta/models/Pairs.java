package com.fluenta.models;

public class Pairs {

	private Long homeTeamId;
	
	private Long awayTeamId;
	
	public Pairs(Long homeTeamId, Long awayTeamId) {
		super();
		this.homeTeamId = homeTeamId;
		this.awayTeamId = awayTeamId;
	}

	public Long getHomeTeamId() {
		return homeTeamId;
	}

	public void setHomeTeamId(Long homeTeamId) {
		this.homeTeamId = homeTeamId;
	}

	public Long getAwayTeamId() {
		return awayTeamId;
	}

	public void setAwayTeamId(Long awayTeamId) {
		this.awayTeamId = awayTeamId;
	}

	@Override
	public String toString() {
		return "Pairs [homeTeamId=" + homeTeamId + ", awayTeamId=" + awayTeamId + "]";
	}
	
		
}
