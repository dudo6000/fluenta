package com.fluenta.models;

import java.util.Comparator;
import java.util.concurrent.ThreadLocalRandom;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
public class TeamResult {
	
	private Long teamId;
	
	private int points;
	
	private int matchesWon;
	
	private int scoredGoals;
	
	private int goalsDifference;

	@Override
	public String toString() {
		return "TeamResult [teamId=" + teamId + ", points=" + points + ", matchesWon=" + matchesWon + ", scoredGoals="
				+ scoredGoals + ", goalsDifference=" + goalsDifference + "]\n";
	}

	
}
