package com.fluenta.models;

import java.util.Comparator;
import java.util.concurrent.ThreadLocalRandom;

public class TeamSortingComparator implements Comparator<TeamResult> { 
	  
    @Override
    public int compare(TeamResult team1, TeamResult team2) { 

    	int higherResult = Integer.compare(team1.getPoints(), team2.getPoints());
    	
    	if(higherResult!=0) return higherResult*-1;
    	        	
    	int winnerMatches = Integer.compare(team1.getMatchesWon(), team2.getMatchesWon());
    	
    	if(winnerMatches!=0) return winnerMatches*-1;
    	        	
    	int scoredGoals = Integer.compare(team1.getScoredGoals(), team2.getScoredGoals());
    	
    	if(scoredGoals!=0) return scoredGoals*-1;
    	        	
    	int scoredGoalsDifference = Integer.compare(team1.getGoalsDifference(), team2.getGoalsDifference());
    	
    	if(scoredGoalsDifference!=0) return scoredGoalsDifference*-1;
    	        	
    	int rand = ThreadLocalRandom.current().nextInt(1, 101);
    	System.out.println("generated: " + rand);
    	return rand%2==1 ? 1 : -1;
    	
    } 
}
